definitions:
  services:
    docker:
      memory: 2048
  caches:
    yarncustom: /root/.yarn-cache

  steps:
    - step: &pureplay-build-dev
        caches:
          - node
          - yarncustom
        image: node:12
        name: Pureplay Dev Build & Test
        script:
          - rm -rf node_modules/
          - node -v
          - npm -v
          - yarn --version
          - apt update && apt install -yq gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
          - yarn config set cache-folder /root/.yarn-cache
          - yarn install --pure-lockfile
          - CI=false yarn build -- --env.dev
          - yarn gulp
          - yarn test
        artifacts:
          - build/**
          - coverage/**
    - step: &pureplay-build-stg
        caches:
          - node
          - yarncustom
        image: node:12
        name: Pureplay Staging Build & Test
        script:
          - rm -rf node_modules/
          - node -v
          - npm -v
          - yarn --version
          - yarn config set cache-folder /root/.yarn-cache
          - yarn install --pure-lockfile
          - CI=false yarn build -- --env.staging
          - yarn gulp
          - yarn test
        artifacts:
          - build/**
          - coverage/**
    - step: &pureplay-build-prd
        caches:
          - node
          - yarncustom
        image: node:12
        name: Pureplay Production Build & Test
        script:
          - rm -rf node_modules/
          - node -v
          - npm -v
          - yarn --version
          - yarn config set cache-folder /root/.yarn-cache
          - yarn install --pure-lockfile
          - CI=false yarn build -- --env.production
          - yarn gulp
          - yarn test
        artifacts:
          - build/**
          - coverage/**
    - step: &default-pipeline
        caches:
          - node
        image: node:12
        name: default-pipeline
        script:
          - npm -v
    - step: &pureplay-deploy-dev
        caches:
          - node
        image: atlassian/pipelines-awscli
        name: Pureplay Dev Deploy
        script:
          - mkdir -p ~/.aws
          - echo -e "[default]\naws_access_key_id = $AWS_CENTRAL_ACCESS_KEY_ID\naws_secret_access_key = $AWS_CENTRAL_SECRET_ACCESS_KEY" > ~/.aws/credentials
          - echo -e "[profile acm-non-prod]\nrole_arn = arn:aws:iam::453489441200:role/bitbucket-pipelines\nsource_profile = default" > ~/.aws/config
          - export AWS_DEFAULT_PROFILE=acm-non-prod
          - aws s3 cp build/ s3://$PUREPLAY_BUCKET_DEV --recursive --exclude "*.js" --exclude "*.css"
          - aws s3 cp build/ s3://$PUREPLAY_BUCKET_DEV --recursive --exclude "*" --include "*.js" --include "*.css" --content-encoding gzip
          - aws cloudfront create-invalidation --distribution-id $PUREPLAY_CF_DEV --paths "/*"
    - step: &performance-test-dev
        name: Web Performance Profiling
        image: justinribeiro/lighthouse
        script:
          - mkdir reports
          - lighthouse --chrome-flags="--headless --disable-gpu --no-sandbox --disable-setuid-sandbox" https://pureplay-dev.pink.cat/order-summary --output html --output-path ./reports/reports-order-${BITBUCKET_BUILD_NUMBER}.html
          - lighthouse --chrome-flags="--headless --disable-gpu --no-sandbox --disable-setuid-sandbox" https://pureplay-dev.pink.cat/order-summary?status=success --output html --output-path ./reports/reports-payment-${BITBUCKET_BUILD_NUMBER}.html
          - lighthouse --chrome-flags="--headless --disable-gpu --no-sandbox --disable-setuid-sandbox" https://pureplay-dev.pink.cat/invalid?type=email --output html --output-path ./reports/reports-invalid-${BITBUCKET_BUILD_NUMBER}.html
        artifacts:
          - reports/**
    - step: &upload-performance-reports
        caches:
          - node
        image: atlassian/pipelines-awscli
        name: Upload Performance Reports
        script:
          - DIR=$(date +%Y%m%d_%H%M%S)
          - mkdir ./$DIR
          - mv reports/*.html ./$DIR
          - mkdir -p ~/.aws
          - echo -e "[default]\naws_access_key_id = $AWS_CENTRAL_ACCESS_KEY_ID\naws_secret_access_key = $AWS_CENTRAL_SECRET_ACCESS_KEY" > ~/.aws/credentials
          - echo -e "[profile acm-non-prod]\nrole_arn = arn:aws:iam::453489441200:role/bitbucket-pipelines\nsource_profile = default" > ~/.aws/config
          - export AWS_DEFAULT_PROFILE=acm-non-prod
          - aws s3 sync ./$DIR s3://acm-performance-reports/pureplay/lighthouse/$DIR
    - step: &security-scan-dev
        name: Security Scan
        image: owasp/zap2docker-weekly
        script:
          - zap-baseline.py -I -t https://pureplay-dev.pink.cat -r zap-${BITBUCKET_BUILD_NUMBER}.html
        services:
          - docker
        caches:
          - docker # adds docker layer caching
        artifacts:
          - zap-*.html
    - step: &pureplay-deploy-stg
        caches:
          - node
        image: atlassian/pipelines-awscli
        name: Pureplay Staging Deploy
        script:
          - mkdir -p ~/.aws
          - echo -e "[default]\naws_access_key_id = $AWS_CENTRAL_ACCESS_KEY_ID\naws_secret_access_key = $AWS_CENTRAL_SECRET_ACCESS_KEY" > ~/.aws/credentials
          - echo -e "[profile acm-non-prod]\nrole_arn = arn:aws:iam::453489441200:role/bitbucket-pipelines\nsource_profile = default" > ~/.aws/config
          - export AWS_DEFAULT_PROFILE=acm-non-prod
          - aws s3 cp build/ s3://$PUREPLAY_BUCKET_STG --recursive --exclude "*.js" --exclude "*.css"
          - aws s3 cp build/ s3://$PUREPLAY_BUCKET_STG --recursive --exclude "*" --include "*.js" --include "*.css" --content-encoding gzip
          - aws cloudfront create-invalidation --distribution-id $PUREPLAY_CF_STG --paths "/*"
    - step: &performance-test-stg
        name: Web Performance Profiling
        image: justinribeiro/lighthouse
        script:
          - lighthouse --chrome-flags="--headless --disable-gpu" https://pureplay-stg.pink.cat/ --output json --output-path ./reports-${BITBUCKET_BUILD_NUMBER}.json
        artifacts:
          - reports-*.json
    - step: &pureplay-deploy-prd
        caches:
          - node
        image: atlassian/pipelines-awscli
        name: Pureplay Production Deploy
        script:
          - mkdir -p ~/.aws
          - echo -e "[default]\naws_access_key_id = $AWS_CENTRAL_ACCESS_KEY_ID\naws_secret_access_key = $AWS_CENTRAL_SECRET_ACCESS_KEY" > ~/.aws/credentials
          - echo -e "[profile acm-prod]\nrole_arn = arn:aws:iam::644909765496:role/bitbucket-pipelines\nsource_profile = default" >> ~/.aws/config
          - export AWS_DEFAULT_PROFILE=acm-prod
          - aws s3 cp build/ s3://$PUREPLAY_BUCKET_PROD --recursive --exclude "*.js" --exclude "*.css"
          - aws s3 cp build/ s3://$PUREPLAY_BUCKET_PROD --recursive --exclude "*" --include "*.js" --include "*.css" --content-encoding gzip
          - aws cloudfront create-invalidation --distribution-id $PUREPLAY_CF_PROD --paths "/*"
    - step: &scan
        image: astrodevops/sonar-scanner:latest
        name: sonarscan
        script:
          - export SONAR_URL=http://13.228.70.214:9000
          - export SONAR_TOKEN=$SONAR_TOKEN
          - export SONAR_ANALYSIS_MODE=publish
          - bitbucket-sonar-scanner


pipelines:
  default:
    - step: *default-pipeline
    - step: *scan

  branches:
    develop:
      - step: *pureplay-build-dev
      - step: *scan
      - step: *pureplay-deploy-dev
      - step: *performance-test-dev
      - step: *upload-performance-reports
      - step: *security-scan-dev
    master:
      - step: *pureplay-build-stg
      - step: *scan
      - step: *pureplay-deploy-stg

  tags:
    v*:
      - step: *pureplay-build-prd
      - step: *scan
      - step: *pureplay-deploy-prd

