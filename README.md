<h1> Setel Front End </h1>

Project is bootstrapped with Create-React-App written in typescript
State is handled using redux and saga is used to handle side effects

`yarn && yarn start` to run the code

### Pending Items
<ul>
<li>Test</li>
<li>Performance audits</li>
<li>Clean up on ui and ux</li>
<li>pipelines</li>
</ul>


### Environment file is required

The environments are;
<ul>
<li>REACT_APP_TOKEN_SECRET : <i>Any string to encode JWT</i></li>
<li>REACT_APP_API_ENDPOINT : <i>Endpoint for apis, given by the terraform output</i></li>
</ul>
