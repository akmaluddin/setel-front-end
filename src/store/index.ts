import { applyMiddleware, createStore } from 'redux'
import { createBrowserHistory } from 'history'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { reducers, rootSaga } from 'ducks'

const history = createBrowserHistory()
const sagaMiddleware = createSagaMiddleware()
const middlewares = [sagaMiddleware]

export default function configureStore () {
  const composeEnhancers = composeWithDevTools({ trace: true })

  const enhancer = composeEnhancers(
    applyMiddleware(...middlewares)
  )

  const store = createStore(reducers, enhancer)

  sagaMiddleware.run(rootSaga);

  return store
}

export { history }
