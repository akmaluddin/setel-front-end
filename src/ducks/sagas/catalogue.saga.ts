import axios from 'axios'
import { all, fork, takeEvery, put, call } from 'redux-saga/effects'
import {
  actionConstants,
  getCatalogueSuccessAction,
  getCatalogueFailedAction
} from 'ducks/actions/catalogue.actions'

function getCatalogue (): Promise<any> {
  return new Promise((resolve, reject) => {
    try {
      const response = axios.get(
				`${process.env.REACT_APP_API_ENDPOINT}/catalogue` || ''
      )
      return resolve(response)
    } catch (error) {
      return reject(error)
    }
  })
}

function * getCatalogueAsync () {
  try {
    const response = yield call(getCatalogue)
    yield put(getCatalogueSuccessAction(response.data.response.catalogue))
  } catch (error) {
    console.error(error)
    yield put(getCatalogueFailedAction())
  }
}

function * getCatalogueFork () {
  yield takeEvery(actionConstants.GET_CATALOGUE, getCatalogueAsync)
}

export default function * rootSaga () {
  yield all([fork(getCatalogueFork)])
}
