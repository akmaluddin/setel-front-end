import { all, fork, takeEvery, put } from 'redux-saga/effects'
import {
  actionConstants,
  setUserAction,
  AuthActionTypes
} from 'ducks/actions/auth.actions'
import Cookies from 'js-cookie'
import * as jwt from 'jsonwebtoken'

function * setAuthAsync ({ payload }: AuthActionTypes) {
  try {
    const usertoken = jwt.sign(payload, process.env.REACT_APP_TOKEN_SECRET)
    yield Cookies.set('userid', usertoken, {
      expires: 1,
      domain: window.location.hostname
    })
    yield put(setUserAction(payload.userid))
  } catch (error) {
    console.error(error)
  }
}

function * setAuthFork () {
  yield takeEvery(actionConstants.SET_USER_COOKIE, setAuthAsync)
}

export default function * rootSaga () {
  yield all([fork(setAuthFork)])
}
