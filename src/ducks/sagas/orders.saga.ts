import axios from "axios";
import { all, fork, takeLatest, put, call } from "redux-saga/effects";
import {
	actionConstants,
	OrdersActionTypes,
	submitOrderSuccessAction,
	submitOrderFailedAction,
	getOrderSuccessAction,
	getOrderFailedAction,
	payOrderSuccessAction,
	pendingOrdersAction,
	payOrderFailedAction
} from "ducks/actions/orders.actions";
import { OrderItem } from "ducks/types/orders.types";
import { CatalogueObject } from "ducks/types/catalogue.types";

function submitOrder(orderItems: OrderItem[], userId: string): Promise<any> {
	return new Promise((resolve, reject) => {
		try {
			const response = axios.post(
				`${process.env.REACT_APP_API_ENDPOINT}/orders` || "",
				{
					orderItems,
					userId
				}
			);
			return resolve(response);
		} catch (error) {
			return reject(error);
		}
	});
}

function getOrder(orderId: string): Promise<any> {
	return new Promise((resolve, reject) => {
		try {
			const response = axios.get(
				`${process.env.REACT_APP_API_ENDPOINT}/orders?id=${orderId}` ||
					""
			);
			return resolve(response);
		} catch (error) {
			return reject(error);
		}
	});
}

function payOrder(orderId: string): Promise<any> {
	return new Promise((resolve, reject) => {
		try {
			const response = axios.get(
				`${process.env.REACT_APP_API_ENDPOINT}/pay?id=${orderId}` || ""
			);
			return resolve(response);
		} catch (error) {
			reject(error);
		}
	});
}

function* submitOrderAsync({ payload }: OrdersActionTypes) {
	try {
		yield put(pendingOrdersAction());
		const orderItems = [];
		Object.entries(payload.orders).map(([key, item]) => {
			orderItems.push({
				itemId: key,
				quantity: (item as CatalogueObject).quantity
			});
		});
		const response = yield call(submitOrder, orderItems, payload.userid);
		const data = response.data.response.entry;
		yield put(
			submitOrderSuccessAction(
				data.order_number,
				data.orderItems,
				data.total,
				data.status
			)
		);
		yield payload.history.push(`/orders/${data.order_number}`);
	} catch (error) {
		console.error(error);
		yield put(submitOrderFailedAction());
	}
}

function* getOrderAsync({ payload }: OrdersActionTypes) {
	try {
		yield put(pendingOrdersAction());
		const response = yield call(getOrder, payload.orderId);
		const data = response.data.response;
		yield put(
			getOrderSuccessAction(
				data.id,
				data.order.total,
				data.order.order_number,
				data.order.userId,
				data.order.orderItems,
				data.order.status
			)
		);
	} catch (error) {
		console.error(error);
		yield put(getOrderFailedAction());
	}
}

function* payOrderAsync({ payload }: OrdersActionTypes) {
	try {
		yield put(pendingOrdersAction());
		const response = yield call(payOrder, payload.orderId);
		const data = response.data.response;
		yield put(payOrderSuccessAction(data.entry.Attributes.status));
	} catch (error) {
		console.error(error);
		yield put(payOrderFailedAction());
	}
}

function* submitOrderFork() {
	yield takeLatest(actionConstants.SUBMIT_ORDER, submitOrderAsync);
}

function* getOrderFork() {
	yield takeLatest(actionConstants.GET_ORDER, getOrderAsync);
}

function* payOrderFork() {
	yield takeLatest(actionConstants.PAY_ORDER, payOrderAsync);
}

export default function* rootSaga() {
	yield all([fork(submitOrderFork), fork(getOrderFork), fork(payOrderFork)]);
}
