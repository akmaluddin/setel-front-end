import { actionConstants, AuthActionTypes } from 'ducks/actions/auth.actions'
import { Reducer } from 'redux'

export interface AuthState {
  userid: string;
}

const initialState: AuthState = {
  userid: ''
}

const authReducer: Reducer<AuthState, AuthActionTypes> = (
  state = initialState,
  actions
) => {
  switch (actions.type) {
    case actionConstants.SET_USER:
      return {
        ...state,
        ...actions.payload
      }

    default:
      return state
  }
}

export default authReducer
