import {
  actionConstants,
  CatalogueActionTypes
} from 'ducks/actions/catalogue.actions'
import { Reducer } from 'redux'

import { CatalogueItem } from 'ducks/types/catalogue.types'
import { Status } from 'ducks/types'

export interface CatalogueState {
  readonly items?: CatalogueItem;
  readonly status: Status;
}

const initialState: CatalogueState = { status: Status.Null }

const catalogueReducer: Reducer<CatalogueState, CatalogueActionTypes> = (
  state = initialState,
  actions
) => {
  switch (actions.type) {
    case actionConstants.GET_CATALOGUE_SUCCESS:
      return {
        ...state,
        ...actions.payload,
        status: Status.Fetched
      }

    case actionConstants.GET_CATALOGUE_FAILED:
      return {
        ...state,
        items: undefined
      }

    default:
      return state
  }
}

export default catalogueReducer
