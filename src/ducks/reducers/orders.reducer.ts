import {
  actionConstants,
  OrdersActionTypes
} from 'ducks/actions/orders.actions'
import { Reducer } from 'redux'
import { OrderItem, OrderStatus } from 'ducks/types/orders.types'
import { Status } from 'ducks/types'

export interface OrdersState {
readonly orderNumber?: number;
readonly orderItems?: OrderItem[];
readonly total?: number;
readonly status: OrderStatus;
readonly apiStatus: Status;
}

const initialState: OrdersState = {
  status: OrderStatus.null,
  apiStatus: Status.Null
}

const ordersReducer: Reducer<OrdersState, OrdersActionTypes> = (
  state = initialState,
  actions
) => {
  switch (actions.type) {
    case actionConstants.GET_ORDER_SUCCESS:
      return {
        ...state,
        ...actions.payload
      }
    case actionConstants.SUBMIT_ORDER_SUCCESS:
      return {
        ...state,
        ...actions.payload,
        apiStatus: Status.Fetched
      }

    case actionConstants.PAY_ORDER_SUCCESS:
      return {
        ...state,
        status: actions.payload.status,
        apiStatus: Status.Fetched
      }

    case actionConstants.PAY_ORDER_FAILED:
      return {
        ...state,
        status: OrderStatus.failed,
        apiStatus: Status.Failed
      }

    case actionConstants.PENDING_RESPONSE:
      return {
        ...state,
        status: OrderStatus.pending,
        apiStatus: Status.Pending
      }

    case actionConstants.DISCARD_ORDERS:
      return {
        status: OrderStatus.null
      }

    case actionConstants.SUBMIT_ORDER_FAILED:
      return {
        status: OrderStatus.failed,
        apiStatus: Status.Failed
      }

    default:
      return state
  }
}

export default ordersReducer
