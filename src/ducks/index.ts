import { combineReducers } from 'redux'
import { all, fork } from 'redux-saga/effects';

import authReducer from './reducers/auth.reducer'
import authSaga from './sagas/auth.saga'

import catalogueReducer from './reducers/catalogue.reducer'
import catalogueSaga from './sagas/catalogue.saga'

import ordersReducer from './reducers/orders.reducer'
import ordersSaga from './sagas/orders.saga'

export const reducers = combineReducers({
  auth: authReducer,
  catalogue: catalogueReducer,
  orders: ordersReducer
})


export function * rootSaga () {
	yield all([fork(authSaga), fork(catalogueSaga), fork(ordersSaga)])
}