export interface OrderItems {
	orderItems: OrderItem[];
}

export interface OrderItem {
	itemId: string;
	quantity: number;
	price?: number;
}

export enum OrderStatus {
	created = "CREATED",
	pending = "PENDING",
	verifed = "VERIFIED",
	canceled = "CANCELED",
	failed = "FAILED",
	null = "NULL"
}
