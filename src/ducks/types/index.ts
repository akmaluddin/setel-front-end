import { AuthState } from "ducks/reducers/auth.reducer";
import { CatalogueState } from "ducks/reducers/catalogue.reducer";
import { OrdersState } from "ducks/reducers/orders.reducer";

export enum Status {
	Pending = "PENDING",
	Fetched = "FETCHED",
	Failed = "FAILED",
	Null = "NULL"
}

export interface State {
	auth: AuthState;
	catalogue?: CatalogueState;
	orders?: OrdersState;
}
