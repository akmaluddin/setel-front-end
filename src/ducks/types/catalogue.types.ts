export interface CatalogueResponse {
	responseMessage: string;
	response: {
		catalogue: CatalogueItem;
	};
}

export interface CatalogueItem {
	[key: string]: {
		name: string;
		price: number;
		status: string;
		quantity?: number;
		total?: number;
	};
}

export interface CatalogueObject {
	name: string;
	price: number;
	status: string;
	quantity?: number;
	total?: number;
}
