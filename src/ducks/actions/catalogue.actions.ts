import { Action, ActionCreator } from 'redux'
import { CatalogueItem } from 'ducks/types/catalogue.types'

// Action Constants

const GET_CATALOGUE = 'catalogue/GET_CATALOGUE'
const GET_CATALOGUE_SUCCESS = 'catalogue/GET_CATALOGUE_SUCCESS'
const GET_CATALOGUE_FAILED = 'catalogue/GET_CATALOGUE_FAILED'

export const actionConstants = {
  GET_CATALOGUE,
  GET_CATALOGUE_SUCCESS,
  GET_CATALOGUE_FAILED
}

// Action Interfaces

export interface GetCatalogueSuccessActionInterface
  extends Action<typeof GET_CATALOGUE_SUCCESS> {
  payload: {
    items: CatalogueItem;
  };
}

export type GetCatalogueActionInterface = Action<typeof GET_CATALOGUE>

export type GetCatalogueFailedActionInterface = Action<typeof GET_CATALOGUE_FAILED>

// Action Type

export interface CatalogueActionTypes extends Action {
  type: string;
  payload?: any;
}

// Actions

export const getCatalogueAction: ActionCreator<GetCatalogueActionInterface> = () => ({
  type: GET_CATALOGUE
})

export const getCatalogueSuccessAction: ActionCreator<GetCatalogueSuccessActionInterface> = (
  items: CatalogueItem
) => ({
  type: GET_CATALOGUE_SUCCESS,
  payload: { items }
})

export const getCatalogueFailedAction: ActionCreator<GetCatalogueFailedActionInterface> = () => ({
  type: GET_CATALOGUE_FAILED
})
