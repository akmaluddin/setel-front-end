import { Action, ActionCreator } from 'redux'
import { CatalogueItem } from 'ducks/types/catalogue.types'
import { OrderItem, OrderStatus } from 'ducks/types/orders.types'

// Action Constants

const SUBMIT_ORDER = 'orders/SUBMIT_ORDER'
const SUBMIT_ORDER_SUCCESS = 'orders/SUBMIT_ORDER_SUCCESS'
const SUBMIT_ORDER_FAILED = 'orders/SUBMIT_ORDER_FAILED'

const GET_ORDER = 'orders/GET_ORDER'
const GET_ORDER_SUCCESS = 'orders/GET_ORDER_SUCCESS'
const GET_ORDER_FAILED = 'orders/GET_ORDER_FAILED'

const PAY_ORDER = 'orders/PAY_ORDER'
const PAY_ORDER_SUCCESS = 'orders/PAY_ORDER_SUCCESS'
const PAY_ORDER_FAILED = 'orders/PAY_ORDER_FAILED'

const PENDING_RESPONSE = 'orders/PENDING_RESPONSE'
const DISCARD_ORDERS = 'orders/DISCARD_ORDERS'

export const actionConstants = {
  SUBMIT_ORDER,
  SUBMIT_ORDER_SUCCESS,
  SUBMIT_ORDER_FAILED,
  GET_ORDER,
  GET_ORDER_SUCCESS,
  GET_ORDER_FAILED,
  PAY_ORDER,
  PAY_ORDER_SUCCESS,
  PAY_ORDER_FAILED,
  PENDING_RESPONSE,
  DISCARD_ORDERS
}

// Action Interfaces

/* eslint-disable camelcase, @typescript-eslint/camelcase */

export interface SubmitOrderActionInterface
  extends Action<typeof SUBMIT_ORDER> {
  payload: {
    orders: CatalogueItem;
    userid: string;
  };
}

export interface SubmitOrderSuccessActionInterface
  extends Action<typeof SUBMIT_ORDER_SUCCESS> {
  payload: {
    orderNumber: number;
    orderItems: OrderItem[];
    total: number;
    status: OrderStatus;
  };
}

export type SubmitOrderFailedActionInterface = Action<
  typeof SUBMIT_ORDER_FAILED
>;

export interface GetOrderActionInterface extends Action<typeof GET_ORDER> {
  payload: {
    orderId: string;
  };
}

export interface GetOrderSuccessActionInterface
  extends Action<typeof GET_ORDER_SUCCESS> {
  payload: {
    id: string;
    total: number;
    order_number: number;
    userId: string;
    orderItems: OrderItem[];
  };
}

export type GetOrderFailedActionInterface = Action<typeof GET_ORDER_FAILED>;

export interface PayOrderActionInterface extends Action<typeof PAY_ORDER> {
  payload: {
    orderId: string;
  };
}

export interface PayOrderSuccessActionInterface
  extends Action<typeof PAY_ORDER_SUCCESS> {
  payload: {
    status: string;
  };
}

export type PayOrderFailedActionInterface = Action<typeof PAY_ORDER_FAILED>;

export type PendingOrdersActionInterface = Action<typeof PENDING_RESPONSE>;

export type DiscardOrdersActionInterface = Action<typeof DISCARD_ORDERS>;

// Action Type

export interface OrdersActionTypes extends Action {
  type: string;
  payload: any;
}

// Actions

export const getOrderAction: ActionCreator<GetOrderActionInterface> = (
  orderId: string
) => ({
  type: GET_ORDER,
  payload: {
    orderId
  }
})

export const getOrderSuccessAction: ActionCreator<GetOrderSuccessActionInterface> = (
  id: string,
  total: number,
  order_number: number,
  userId: string,
  orderItems: OrderItem[],
  status: OrderStatus
) => ({
  type: GET_ORDER_SUCCESS,
  payload: {
    id,
    total,
    order_number,
    userId,
    orderItems,
    status
  }
})

export const getOrderFailedAction: ActionCreator<GetOrderFailedActionInterface> = () => ({
  type: GET_ORDER_FAILED
})

export const submitOrderAction: ActionCreator<SubmitOrderActionInterface> = (
  orders: CatalogueItem,
  userid: string,
  history?: any
) => ({
  type: SUBMIT_ORDER,
  payload: {
    orders,
    userid,
    history
  }
})

export const submitOrderSuccessAction: ActionCreator<SubmitOrderSuccessActionInterface> = (
  orderNumber: number,
  orderItems: OrderItem[],
  total: number,
  status: OrderStatus
) => ({
  type: SUBMIT_ORDER_SUCCESS,
  payload: {
    orderNumber,
    orderItems,
    total,
    status
  }
})

export const submitOrderFailedAction: ActionCreator<SubmitOrderFailedActionInterface> = () => ({
  type: SUBMIT_ORDER_FAILED
})

export const payOrderAction: ActionCreator<PayOrderActionInterface> = (
  orderId: string
) => ({
  type: PAY_ORDER,
  payload: {
    orderId
  }
})

export const payOrderSuccessAction: ActionCreator<PayOrderSuccessActionInterface> = (
  status: string
) => ({
  type: PAY_ORDER_SUCCESS,
  payload: {
    status
  }
})

export const payOrderFailedAction: ActionCreator<PayOrderFailedActionInterface> = () => ({
  type: PAY_ORDER_FAILED
})

export const pendingOrdersAction: ActionCreator<PendingOrdersActionInterface> = () => ({
  type: PENDING_RESPONSE
})

export const discardOrdersAction: ActionCreator<DiscardOrdersActionInterface> = () => ({
  type: DISCARD_ORDERS
})
