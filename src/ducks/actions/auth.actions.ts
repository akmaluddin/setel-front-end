import { Action, ActionCreator } from 'redux'

// Action Constants

const SET_USER = 'auth/SET_USER'
const SET_USER_COOKIE = 'auth/SET_USER_COOKIE'

export const actionConstants = {
  SET_USER,
  SET_USER_COOKIE
}

// Action Interfaces

export interface SetUserActionInterface extends Action<typeof SET_USER> {
  payload: {
    userid: string;
  };
}
export interface SetUserCookieActionInterface
  extends Action<typeof SET_USER_COOKIE> {
  payload: {
    userid: string;
  };
}

// Action Type

export type AuthActionTypes =
  | SetUserActionInterface
  | SetUserCookieActionInterface;

// Actions

export const setUserAction: ActionCreator<SetUserActionInterface> = (
  userid: string
) => ({
  type: SET_USER,
  payload: {
    userid
  }
})
export const setUserCookieAction: ActionCreator<
  SetUserCookieActionInterface
> = (userid: string) => ({
  type: SET_USER_COOKIE,
  payload: {
    userid
  }
})
