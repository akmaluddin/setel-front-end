import React, { FunctionComponent } from 'react'
import './index.scss'

const key = 'Cards'

interface CardProps {
	children: any;
}

const Cards: FunctionComponent<CardProps> = props => {
  return (
    <main id={key} className={key}>
      <div className="container">{props.children}</div>
    </main>
  )
}

Cards.displayName = key

export default Cards
