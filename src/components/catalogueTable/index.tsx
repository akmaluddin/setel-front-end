import React, { FunctionComponent, useEffect, useState } from "react";
import { CatalogueItem } from "ducks/types/catalogue.types";
import QuantityButton from "components/quantityButton";
import "./index.scss";

const key = "CatalogueTable";

interface CatalogueTableProps {
  items: CatalogueItem;
  callback: (orders: any[], total: number, summary: any) => void;
}

const CatalogueTable: FunctionComponent<CatalogueTableProps> = props => {
  const { items, callback } = props;
  const [tableEntries, setTableEntries] = useState<any>(undefined);
  const [total, setTotal] = useState<number>(0);

  useEffect(() => {
    console.log("INIT");
    const catalogueItems = {};
    items &&
      Object.entries(items).map(([key, value]) => {
        value.status === "AVAILABLE" &&
          (catalogueItems[key] = { ...value, quantity: 0, total: 0 });
      });
    items && setTableEntries(catalogueItems);
  }, [items]);

  const calculateTotal = () => {
    let totalTemp = 0;
    const orderTemp = [];
    const tableTemp = {};
    Object.entries(tableEntries).map(([key, item]) => {
      totalTemp += Number(item["total"]);
      if (item["quantity"] !== 0) {
        orderTemp.push({
          itemId: key,
          quantity: item["quantity"]
        });
        tableTemp[key]=item
      }
    });

    callback(orderTemp, totalTemp, tableTemp);
  };

  const callbackButton = (itemKey: string, value: number) => {
    const temp = tableEntries;
    temp[itemKey] = {
      ...temp[itemKey],
      quantity: value,
      total: Number(value * temp[itemKey].price)
    };
    setTableEntries({ ...temp });
    calculateTotal();
  };

  return (
    <main id={key} className={key}>
      <table>
        <thead>
          <tr>
            <th style={{ width: "20%", minWidth: "200px" }}></th>
            <th style={{ width: "20%", minWidth: "200px" }}></th>
            <th style={{ width: "5%", minWidth: "120px" }}>Quantity</th>
            <th style={{ width: "3%", minWidth: "180px" }}>Total</th>
          </tr>
        </thead>
        <tbody>
          {tableEntries &&
            Object.entries(tableEntries).map(([key, item]) => {
              return (
                <tr key={key}>
                  <td>{item["name"]}</td>
                  <td>{`RM ${Number(item["price"]).toFixed(2)}`}</td>
                  <td>
                    {
                      <QuantityButton
                        itemKey={key}
                        value={item["quantity"]}
                        callback={callbackButton}
                      />
                    }
                  </td>
                  <td>{`RM ${Number(item["total"]).toFixed(2)}`}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
      <div></div>
    </main>
  );
};

CatalogueTable.displayName = key;

export default CatalogueTable;
