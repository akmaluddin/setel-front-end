import React, { FunctionComponent, useState, useCallback } from "react";
import "./index.scss";

const key = "QuantityButton";

interface QuantityButtonProps {
	itemKey: string;
	value: number;
	callback?: (key: string, updatedValue: number) => void;
}

const QuantityButton: FunctionComponent<QuantityButtonProps> = props => {
	const { itemKey, value, callback } = props;
	const [updatedValue, setUpdatedValue] = useState(value);

	const update = useCallback((itemKey_: string, value_: number) => {
		callback(itemKey_, value_);
	}, []);

	const subtractValue = () => {
		setUpdatedValue(updatedValue - 1);
		callback(itemKey, value-1);
	};

	const addValue = () => {
		setUpdatedValue(updatedValue + 1);
		callback(itemKey, value+1);
	};

	return (
		<main id={key} className={key}>
			<span>
				<button onClick={subtractValue} disabled={value === 0}><i className={`zmdi zmdi-minus-circle-outline ${value === 0 ? 'zmdi-disabled' : null}`}/></button>
				{value}
				<button onClick={addValue} disabled={value === 20}><i className={`zmdi zmdi-plus-circle-o ${value === 20 ? 'zmdi-disabled' : null}`}/></button>
			</span>
		</main>
	);
};
export default QuantityButton;
