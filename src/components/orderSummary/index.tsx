import React, { FunctionComponent } from "react";
import { CatalogueItem } from "ducks/types/catalogue.types";
import "./index.scss";

const key = "OrderSummary";

interface OrderSummaryProps {
	summary: CatalogueItem;
	total: number;
}

const OrderSummary: FunctionComponent<OrderSummaryProps> = props => {
	const { summary, total } = props;

	const transform = () => {
		if (summary) {
			return (
				<div className="column">
					<div className="row header">
						<div className="item main left">Product Name</div>
						<div className="item">Quantity</div>
						<div className="item">Price Per Item</div>
						<div className="item main right">Total</div>
					</div>
					{Object.entries(summary).map(([key, item]) => {
						return (
							<div className="row" key={key}>
								<div className="item main left">
									{item["name"]}
								</div>
								<div className="item">{item["quantity"]}</div>
								<div className="item">
									{Number(item["price"]).toFixed(2)}
								</div>
								<div className="item main right">
									{Number(item["total"]).toFixed(2)}
								</div>
							</div>
						);
					})}
					<div className="row footer" key={key}>
						<div className="item main left">Total</div>
						<div className="item"></div>
						<div className="item"></div>
						<div className="item main right">
							{Number(total).toFixed(2)}
						</div>
					</div>
				</div>
			);
		}
		return <React.Fragment />;
	};

	return (
		<main id={key} className={key}>
			<div>{transform()}</div>
		</main>
	);
};

OrderSummary.displayName = key;

export default OrderSummary;
