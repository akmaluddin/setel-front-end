import React, {
  FunctionComponent,
  lazy,
  useEffect,
  useState
} from "react";
import {
  Switch,
  Route,
  withRouter,
  Redirect,
  RouteComponentProps
} from "react-router";
import Cookies from "js-cookie";
import { connect } from "react-redux";
import * as jwt from "jsonwebtoken";
import { setUserAction } from "ducks/actions/auth.actions";
import { State } from "ducks/types";

const Login = lazy(() => import("pages/login"));
const Catalogue = lazy(() => import("pages/catalogue"));
const Orders = lazy(() => import("pages/orders"));

export const PrivateRoutes: FunctionComponent<any> = ({
  component: Component,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={() =>
        rest.loggedIn ? <Component {...rest} /> : <Redirect to="/login" />
      }
    />
  );
};

interface PagesProps extends RouteComponentProps, State {
  setUserAction: (username: string) => void;
}

const Pages: FunctionComponent<PagesProps> = props => {
  const { setUserAction, auth } = props;
  const [loggedIn, setLoggedIn] = useState<boolean | undefined>(undefined);

  useEffect(() => {
    setLoggedIn(!!Cookies.get("userid"));
    const cookie = Cookies.get("userid");
    if (cookie !== undefined && auth.userid === "") {
      const decodedUsername = jwt.decode(cookie);
      setUserAction((decodedUsername as any).userid);
    }
  });

  return loggedIn !== undefined ? (
    <Switch>
      <Route
        path="/login"
        render={() => {
          if (!loggedIn) {
            return <Login />;
          } else {
            return <Redirect to="/catalogue" />;
          }
        }}
      />
      <PrivateRoutes
        path="/catalogue"
        component={Catalogue}
        loggedIn={loggedIn}
      />
      <PrivateRoutes
        path={["/orders/:id", "/orders"]}
        component={Orders}
        loggedIn={loggedIn}
      />
      <Redirect from="*" to="/login" />
    </Switch>
  ) : (
    <React.Fragment/>
  );
};

const mapStateToProps = (state: State) => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = {
  setUserAction
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Pages));
