import React, {
  FunctionComponent,
  useEffect,
  useState
} from 'react'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import {
  submitOrderAction,
  getOrderAction,
  discardOrdersAction,
  payOrderAction
} from 'ducks/actions/orders.actions'
import { getCatalogueAction } from 'ducks/actions/catalogue.actions'
import { CatalogueItem } from 'ducks/types/catalogue.types'
import { State, Status } from 'ducks/types'

import Cards from 'components/cards'
import OrderSummary from 'components/orderSummary'
import './index.scss'

const key = 'Orders'

interface OrdersProps extends State, RouteComponentProps {
	submitOrderAction: (
		orders: CatalogueItem,
		userid: string,
		history: any
	) => void;
	getOrderAction: (orderId: string) => void;
	discardOrdersAction: () => void;
	payOrderAction: (orderId: string) => void;
	getCatalogueAction: () => void;
}

interface MatchParams {
	id?: string;
}

const Orders: FunctionComponent<OrdersProps> = props => {
  const {
    history,
    match,
    submitOrderAction,
    auth,
    orders,
    catalogue,
    getOrderAction,
    payOrderAction,
    discardOrdersAction,
    getCatalogueAction
  } = props
  const [orderSummary, setOrderSummary] = useState<any>(undefined)
  const [total, setTotal] = useState<number>(0)

  const handleDiscard = () => {
    discardOrdersAction()
    history.push('/catalogue')
  }

  useEffect(() => {
    if (history.location.state === undefined && (match.params as MatchParams).id === undefined) {
      handleDiscard()
    }
  }, [match])

  useEffect(() => {
    history.location.state &&
			setOrderSummary((history.location.state as any).summary)
    history.location.state &&
			setTotal((history.location.state as any).totalOrder)
  }, [history])

  useEffect(() => {
    if (match.params && ((match.params as any).id as string)) {
      getOrderAction((match.params as any).id)
    }
  }, [match])

  useEffect(() => {
    if (orders.status === 'CREATED' && catalogue.status === 'FETCHED') {
      const orderSummaryTemp = {}
      orders.orderItems &&
				orders.orderItems.map(item => {
				  const key = item.itemId
				  orderSummaryTemp[key] = {
				    name: catalogue.items[key].name,
				    price: item.price,
				    quantity: item.quantity,
				    total: Number(item.price * item.quantity).toFixed(
				      2
				    )
				  }
				})
      setOrderSummary(orderSummaryTemp)
      setTotal(orders.total)
    } else if (catalogue.status === 'NULL') {
      getCatalogueAction()
    }
  }, [orders, catalogue])

  const showButtons = () => {
    if (history.location.state) {
      return (
        <React.Fragment>
          <button
            className="buttonSubmit"
            onClick={() =>
              submitOrderAction(
                orderSummary,
                auth.userid,
                history
              )
            }
          >
            {`${orders.apiStatus === Status.Failed ? 'Retry' : 'Submit Order'}`}
          </button>
          <button
            className="buttonDiscard"
            onClick={() => {
              handleDiscard()
            }}
          >
						Discard
          </button>
        </React.Fragment>
      )
    } else if (orders.status === 'CREATED') {
      return (
        <React.Fragment>
          <button
            className="buttonSubmit pay"
            onClick={() => payOrderAction((match.params as MatchParams).id)}
          >
            {`${orders.apiStatus === Status.Failed ? 'Retry' : `Pay RM ${orders.total}`}`}
          </button>
          <button
            className="buttonDiscard"
            onClick={() => {
              handleDiscard()
            }}
          >
						Cancel
          </button>
        </React.Fragment>
      )
    } else if (orders.status === 'VERIFIED') {
      return (
        <React.Fragment>
          <button
            className="buttonSubmit"
            onClick={() => handleDiscard()}
          >
            {'Payment Successful. Return to catalogue'}
          </button>
        </React.Fragment>
      )
    } else if (orders.apiStatus === 'PENDING') {
      return (
        <React.Fragment>
          <div style={{ paddingTop: '20px' }}>
            <i className="zmdi zmdi-refresh-alt" />
          </div>
        </React.Fragment>
      )
    } else if (orders.status === 'FAILED') {
      return (
        <React.Fragment>
          <button
            className="buttonDiscard"
            onClick={() => handleDiscard()}
          >
            {'Payment UnSuccessful. Return to catalogue'}
          </button>
        </React.Fragment>
      )
    }
    return <div />
  }

  return (
    <main id={key} className={key}>
      <Cards>
        <h1>{`Order Summary ${
					match.params && (match.params as MatchParams).id
						? `#${(match.params as any).id}`
						: ''
				}`}</h1>
        <OrderSummary summary={orderSummary} total={total} />
      </Cards>
      {showButtons()}
    </main>
  )
}

Orders.displayName = key

const mapStateToProps = (state: State) => {
  return {
    auth: state.auth,
    orders: state.orders,
    catalogue: state.catalogue
  }
}

const mapDispatchToProps = {
  submitOrderAction,
  getOrderAction,
  discardOrdersAction,
  payOrderAction,
  getCatalogueAction
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Orders))
