import React, { FunctionComponent, useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter, RouteComponentProps } from "react-router";
import { getCatalogueAction } from "ducks/actions/catalogue.actions";
import { State } from "ducks/types";

import Cards from "components/cards";
import CatalogueTable from "components/catalogueTable";
import "./index.scss";

const key = "Catalogue";

interface CatalogueProps extends State, RouteComponentProps {
  getCatalogueAction: () => void;
}

const Catalogue: FunctionComponent<CatalogueProps> = props => {
  const { getCatalogueAction, catalogue, history } = props;

  const [orderToSubmit, setOrder] = useState<any>([]);
  const [totalOrder, setTotal] = useState<number>(0);
  const [summary, setSummary] = useState<any>(undefined);

  useEffect(() => {
    getCatalogueAction();
  }, []);

  const callback = (orders: any[], total: number, summary: any) => {
    setOrder(orders);
    setTotal(Number(total.toFixed(2)));
    setSummary(summary);
  };

  return (
    <main id={key} className={key}>
      <Cards>
        <h1>Catalogue</h1>
        <div
          style={{
            overflow: "scroll"
          }}
        >
          <CatalogueTable
            items={catalogue.items}
            callback={callback}
          ></CatalogueTable>
        </div>
      </Cards>
      <button
        className="buttonSubmit"
        disabled={totalOrder === 0}
        onClick={() => {
          history.push({
            pathname: "/orders",
            state: { order: orderToSubmit, summary: summary, totalOrder }
          });
        }}
      >{`Amount to pay ${totalOrder > 0 ? ` RM ${totalOrder.toFixed(2)}` : ""}`}</button>
    </main>
  );
};

Catalogue.displayName = key;

const mapStateToProps = (state: State) => {
  return {
    catalogue: state.catalogue
  };
};

const mapDispatchToProps = {
  getCatalogueAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Catalogue));
