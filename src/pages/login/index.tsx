import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter, RouteComponentProps } from 'react-router'
import { setUserCookieAction } from 'ducks/actions/auth.actions'

import avatarImg from 'assets/avatar.jpg'
import './index.scss'

const key = 'Login'

interface LoginProps extends RouteComponentProps {
  setUserCookieAction: (username: string) => void;
}

const Login: React.FunctionComponent<LoginProps> = props => {
  const { setUserCookieAction, history } = props
  const [username, setUsername] = useState('')
  const [isValid, setValid] = useState(false)

  useEffect(() => {
    if (username !== '') {
      setValid(true)
    } else {
      setValid(false)
    }
  }, [username])

  const handleChange = (event: any) => {
    setUsername(event.target.value.replace(/[^a-zA-Z]*/g, ''))
  }

  const handleLogin = (username: string) => {
    setUserCookieAction(username)
    history.push('/catalogue')
  }

  return (
    <main id={key} className={key}>
      <div className="Card">
        <img src={avatarImg} className="Avatar" />
        <input
          placeholder="Username"
          value={username}
          onChange={handleChange}
          type="text"
        />
        <span className="Info">
          Registered username is Test, but you may place any username and will
          automatically create order profile for that user
        </span>
        <button onClick={() => handleLogin(username)} disabled={!isValid}>
          Enter
        </button>
      </div>
    </main>
  )
}

Login.displayName = key

const mapDispatchToProps = {
  setUserCookieAction
}

export default connect(null, mapDispatchToProps)(withRouter(Login))
