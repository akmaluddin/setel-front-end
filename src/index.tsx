import React, { Suspense, lazy } from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import * as serviceWorker from "./serviceWorker";
import configureStore, { history } from "store";
import { Router, Route, Switch, Redirect } from "react-router";
import { Provider } from "react-redux";

const Pages = lazy(() => import("pages/index"));

const store = configureStore();

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
			<Suspense fallback={<div>Loading ...</div>}>
				<Pages/>
			</Suspense>
		</Router>
	</Provider>,
	document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
